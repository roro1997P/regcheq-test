import { GetterTree, ActionTree, MutationTree } from 'vuex';

type User = {
  id: number;
  email: string;
  password: string;
  name: string;
  role: string;
  avatar: string;
}

type Login = {
  email: string;
  password: string;
}

type State = {
  user: User;
  authenticated: boolean;
  token: string;
}

const baseURL: string = "https://api.escuelajs.co/api/v1/auth";

export const state = () => ({
  user: null,
  authenticated: null,
  token: null
})

export const getters: GetterTree<State, State> = {
  isAuthenticated() {
    const authenticated = JSON.parse(localStorage.getItem("authenticated")) || false;
    return authenticated;
  },
  getUser(state: State) {
    return state.user;
  }
}

export const mutations: MutationTree<State> = {
  setAuthenticated(state: State, payload: boolean) {
    localStorage.setItem("authenticated", JSON.stringify(payload));
    state.authenticated = payload;
  },
  setUser(state: State, payload: User) {
    state.user = payload;
  },
  setAccessToken(state: State, payload: string) {
    localStorage.setItem("token", JSON.stringify(payload));
    state.token = payload;
  },
  clearUser(state: State) {
    state.authenticated = false;
    state.user = null;
    state.token = null;
    localStorage.removeItem("authenticated");
    localStorage.removeItem("token");
  }
}

export const actions: ActionTree<State, State> = {
  async loginAction ({ commit }, login: Login): Promise<User> {
    try {
      const { access_token } = await this.$axios.$post(baseURL + '/login', login);
      const user: User = await this.$axios.$get(baseURL + '/profile', { headers: { "Authorization": `Bearer ${access_token}`}}) || null;
      
      commit("setAccessToken", access_token);
      commit("setUser", user);
      commit("setAuthenticated", user !== null);

      if(user !== null) {
        window.location.reload();
      }

      return user;
    } catch (error) {

      commit("setUser", null);
      commit("setAuthenticated", false);

      return null;
    }
  },
  logoutAction({ commit }): void {
    commit("clearUser");
    window.location.reload();
  }
}