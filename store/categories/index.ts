import { GetterTree, ActionTree, MutationTree } from 'vuex';

import { Category } from "~/types/categories";

type State = {
  categories: Category[],
  category: Category,
}

const baseURL: string = "https://api.escuelajs.co/api/v1/categories";

export const state = () => ({
  categories: [],
  category: null
})

export const getters: GetterTree<State, State> = {
  getCategories(state: State) {
    return state.categories;
  },
  getCategory(state: State) {
    return state.category;
  }
}

export const mutations: MutationTree<State> = {
  setCategories(state: State, payload: Category[]) {
    state.categories = payload;
  },
  setCategory(state: State, payload: Category) {
    state.category = payload;
  }
}

export const actions: ActionTree<State, State> = {
  async getCategoriesAction ({ commit }): Promise<Category[]> {
    const categories:Category[] = await this.$axios.$get(baseURL) || [];
    
    commit("setCategories", categories);
    return categories;
  },
  async getSingleCategoryAction ({ commit }, id): Promise<Category> {
    const category:Category = await this.$axios.$get(baseURL + `/${id}`) || null;

    commit("setCategory", category);
    return category;
  },

  async updateCategoryAction({ commit }, categoryUpdated: Category): Promise<Category> {
    const category:Category = await this.$axios.$put(baseURL + `/${categoryUpdated.id}`, categoryUpdated) || null;

    commit("setCategory", category);
    return category;
  },

  async createCategoryAction({}, newCategory: Category): Promise<Category> {
    const category:Category = await this.$axios.$post(baseURL, newCategory) || null;

    return category;
  },


  async deleteCategoryAction({}, id): Promise<void> {
    await this.$axios.$delete(baseURL + `/${id}`) || null;
  }
}