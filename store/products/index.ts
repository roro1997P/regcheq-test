import { GetterTree, ActionTree, MutationTree } from 'vuex';

import { TablePagination } from '~/types/common';
import { Product, ProductDataTableFilters } from "~/types/products";

type State = {
  products: Product[],
  product: Product,
}

const baseURL: string = "https://api.escuelajs.co/api/v1/products";

export const state = () => ({
  products: [],
  product: null
})

export const getters: GetterTree<State, State> = {
  getProducts(state: State) {
    return state.products;
  },
  getProduct(state: State) {
    return state.product;
  }
}

export const mutations: MutationTree<State> = {
  setProducts(state: State, payload: Product[]) {
    state.products = payload;
  },
  setProduct(state: State, payload: Product) {
    state.product = payload;
  }
}

export const actions: ActionTree<State, State> = {
  async getTotalProductsAction ({}, filters: ProductDataTableFilters): Promise<number> {
    const products:Product[] = await this.$axios.$get(baseURL, { params: filters }) || [];
    return products ? products.length : 0;
  },
  async getProductsAction ({ commit }, { pagination, filters }: { pagination: TablePagination, filters: ProductDataTableFilters }): Promise<Product[]> {
    const products:Product[] = await this.$axios.$get(baseURL, {
      params: { limit: pagination.limit, offset: pagination.offset, ...filters }
    }) || [];
    
    commit("setProducts", products);
    return products;
  },
  async getSingleProductAction ({ commit }, id): Promise<Product> {
    const product:Product = await this.$axios.$get(baseURL + `/${id}`) || null;

    commit("setProduct", product);
    return product;
  },

  async updateProductAction({ commit }, productUpdated: Product): Promise<Product> {
    const product:Product = await this.$axios.$put(baseURL + `/${productUpdated.id}`, productUpdated) || null;

    commit("setProduct", product);
    return product;
  },

  async createProductAction({}, newProduct: Product): Promise<Product> {
    const product:Product = await this.$axios.$post(baseURL, newProduct) || null;

    return product;
  },


  async deleteProductAction({}, id): Promise<void> {
    await this.$axios.$delete(baseURL + `/${id}`) || null;
  }
}