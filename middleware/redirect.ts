export default function ({ store, redirect }) {
    return redirect(301, '/products');
}