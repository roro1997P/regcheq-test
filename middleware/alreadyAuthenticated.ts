export default function ({ store, redirect }) {
  if (store.getters['authentication/isAuthenticated']) {
    redirect('/products')
  }
}