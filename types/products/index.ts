export interface Product {
	id: number;
	title: string;
	price: number;
	description: string;
	category: Category;
	images: string[];
}

interface Category {
	id: number;
	name: string;
	image: string;
}

export interface ProductTable {
	id: number;
	title: string;
	price: number;
	description: string;
	category: string;
}

export interface DataTableHeaders {
  text: string;
  align?: "start" | "center" | "end";
  sortable?: boolean;
  value: string;
}

export interface ProductDataTableFilters {
	title?: string;
	price?: number;
	price_min?: number;
	price_max?: number;
	categoryId?: number;
}