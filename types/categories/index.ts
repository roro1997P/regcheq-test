export interface Category {
	id: number;
	name: string;
	image: string;
}

export interface CategoryTable {
	id: number;
	name: string;
	image: string;
}

export interface DataTableHeaders {
  text: string;
  align?: "start" | "center" | "end";
  sortable?: boolean;
  value: string;
  filterable?: boolean;
}